﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsShipEnabled : MonoBehaviour {

    public GameObject shipObject;                           //The ship GameObject

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        shipObject.GetComponent<GameObject>();              //This will prevent any movement to the ship when called.
        UpdateShipStatus(shipObject);                       //This will deactivate the sprite of the ship when called.
	}

    public void UpdateShipStatus(GameObject shipObject)
    {
        //This will activate and deactivate the ship.
        if (Input.GetKey(KeyCode.P))
        {
            shipObject.GetComponent<ShipMovement>().enabled = !shipObject.GetComponent<ShipMovement>().enabled;
        }

        //This will activate and deactivate the sprite.
        if (Input.GetKey(KeyCode.Q))
        {
            shipObject.SetActive(false);
        }
        else
        {
            shipObject.SetActive(true);
        }
    }
}
