﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour {

    public GameObject ship;                                 //The main GameObject
    public GameObject sprite;                               //The child of GameObject which will hold the sprite.
    private Rigidbody2D myRigidboby;                        //The Rigidbody to the child of GameObject's Sprite

    //This controls the movement speed of the ship.
    public float moveSpeed;

	// Use this for initialization
	void Start ()
    {
        //This will read the spaceship's GameObject along with the Rigidbody.
        ship.GetComponent<GameObject>();
        myRigidboby = GetComponent<Rigidbody2D>();
    }

	// Update is called once per frame
	void Update ()
    {
        /*
         The two following will control the movement of the spaceship.
         axisX controls left and right movements
         axisy controls up and down movements.
        */
        float axisX = Input.GetAxis("Horizontal");
        float axisY = Input.GetAxis("Vertical");

        HandleMovement(axisX, axisY);                   //This will handle the regular ship's movement.
        HandleAdditiveMovement(axisX, axisY);           //This will handle the 1 meter movements of the ship.
    }

    //This module will handle the movement of the ship.
    public void HandleMovement(float horizonal, float vertical)
    {
        //This will handle left, right, up and down.
        myRigidboby.velocity = new Vector2(horizonal * moveSpeed, vertical * moveSpeed);
        
        //This will reset the position of the ship to the origin.
        if (Input.GetKey(KeyCode.Space))
        {
            myRigidboby.position = new Vector2(0.0f, 0.0f);
        }

        //When the user presses the Espace, the application will exit out.
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    public void HandleAdditiveMovement(float horizontal, float vertical)
    {
        //The Below will follow a slow shift movement when the Shift is held down.
        transform.Translate(new Vector2(horizontal, vertical) * Time.deltaTime * moveSpeed);

        //Move left 1 meter
        if (Input.GetKeyDown(KeyCode.LeftShift) && Input.GetKey(KeyCode.A) ||
           Input.GetKeyDown(KeyCode.A) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(-1.0f, 0.0f, 0.0f);
            moveSpeed = 0.0f;
        }

        //Move right 1 meter
        if (Input.GetKeyDown(KeyCode.LeftShift) && Input.GetKey(KeyCode.D) ||
           Input.GetKeyDown(KeyCode.D) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(1.0f, 0.0f, 0.0f);
            moveSpeed = 0.0f;
        }

        //Move up 1 meter
        if (Input.GetKeyDown(KeyCode.LeftShift) && Input.GetKey(KeyCode.W) ||
           Input.GetKeyDown(KeyCode.W) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(0.0f, 1.0f, 0.0f);
            moveSpeed = 0.0f;
        }

        //Move down 1 meter
        if (Input.GetKeyDown(KeyCode.LeftShift) && Input.GetKey(KeyCode.S) ||
           Input.GetKeyDown(KeyCode.S) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.Translate(0.0f, -1.0f, 0.0f);
            moveSpeed = 0.0f;
        }
    }
}
